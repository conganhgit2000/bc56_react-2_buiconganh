import React, { Component } from "react";

export default class Content extends Component {
  render() {
    let { img } = this.props;
    let imageGlasses = `./glassesImage/${img}.png`;
    return (
      <div className="row">
        <div className="col-6">
          <div
            style={{ width: "550px", position: "relative" }}
            className="img__item"
          >
            <img className="w-75" src="./glassesImage/model.jpg" alt="" />
            <img
              style={{
                width: "34%",
                position: "absolute",
                top: "130px",
                left: "182px",
              }}
              src={imageGlasses}
              alt=""
            />
          </div>
        </div>
        <div className="col-6">
          <div style={{ width: "550px" }} className="img__item">
            <img className="w-75" src="./glassesImage/model.jpg" alt="" />
          </div>
        </div>
        <div
          className=" p-3 col-12 text-left my-4"
          style={{
            backgroundColor: "rgba(0,0,0,0.5)",
          }}
        >
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v1");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g1.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v2");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g2.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v3");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g3.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v4");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g4.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v5");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g5.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v6");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g6.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v7");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g7.jpg"
              alt=""
            />
          </button>
          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v8");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g8.jpg"
              alt=""
            />
          </button>

          <button
            className="col-2 my-3 gap-3"
            style={{ border: "none", background: "none" }}
            onClick={() => {
              this.props.handleChangeGlasses("v9");
            }}
          >
            <img
              style={{
                width: "100px",
                height: "50px",
                objectFit: "cover",
              }}
              src="./glassesImage/g9.jpg"
              alt=""
            />
          </button>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { arrGlasses } from "./data";
import Content from "./Content";

export default class Component_glasses extends Component {
  state = {
    arrGlasses: arrGlasses,
    glasses: "v1",
  };
  handleChangeGlasses = (glasses) => {
    this.setState({ glasses: glasses });
  };
  render() {
    return (
      <div className="container">
        <Content
          img={this.state.glasses}
          handleChangeGlasses={this.handleChangeGlasses}
          list={this.state.arrGlasses}
        />
      </div>
    );
  }
}
